# Wikidata Castle Query

[![build status](https://gitlab.com/geometalab/castle-list-generator-wd/badges/master/pipeline.svg)](https://gitlab.com/geometalab/castle-list-generator-wd/commits/master)

The scripts in this repository query Wikidata for castles. The resulting tags are then searched for on OSM. Tags without a corresponding OSM entry are published [here](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier/FehlendeOSMEintraege).

![Overview](img/wd_query_overview.png)

## Overview

In the following, the scripts involved in the process are listed and briefly described to provide an overview of the application.

* **castle\_query\_wd.py**: This script queries the [Wikidata Sparql Endpoint](https://query.wikidata.org/) for entities with tag castle `wdt:P31 wd:Q23413` in the region Switzerland `wdt:P17 wd:Q39`. The resulting JSON output can be downloaded [here](https://gitlab.com/geometalab/castle-list-generator-wd/-/jobs/artifacts/master/raw/dumps/wd_castles.json?job=deploy).

* **castle\_query\_osm.py**: This script queries the [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API) for nodes, ways and relations with tag `wikidata=???`, where `???` represents a regex matching for all tags of the script above. The resulting JSON output can be downloaded [here](https://gitlab.com/geometalab/castle-list-generator-wd/-/jobs/artifacts/master/raw/dumps/osm_tagged_entities.json?job=deploy).

* **missing\_osm\_entry\_processor.py**: This script combines and filters the JSON outputs produced above. The result is a file containing a wiki page with a list of the required Wikidata entries. The file can be downloaded [here](https://gitlab.com/geometalab/castle-list-generator-wd/-/jobs/artifacts/master/raw/wiki/wd_missing_osm_entities?job=deploy).

* **add\_text\_to\_wiki.py**: This script publishes the contents of the previously produced file on [Wikimedia](https://meta.wikimedia.org/wiki/Wikimedia_CH/Burgen-Dossier/FehlendeOSMEintraege) using a shared [PyWikiBot](https://www.mediawiki.org/wiki/Manual:Pywikibot) with username `OSMCastleBot`. If the bots behaviour causes issues, please create an issue on this repository's issue page.

* **wd\_main.py**: This script executes the above features in the order described. The CI job for this repository is scheduled to run every hour on `HH:12`. The linked artifacts are updated according to this schedule.

## Contribution

If you think you have a valuable contribution, feel free to open a pull request.

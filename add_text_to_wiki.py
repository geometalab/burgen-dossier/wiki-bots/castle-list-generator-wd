import codecs
import pywikibot
from pywikibot import config


def put_text(page, new, summary='', asynchronous=False):
    for _ in range(config.max_retries):
        if _put_text(page, new, summary, asynchronous):
            break
    else:
        raise pywikibot.ServerError('Server Error! Maximum retries exceeded')
    

def _put_text(page, new, summary='', asynchronous=False):
    page.text = new
    try:
        page.save(summary=summary, asynchronous=asynchronous, minor=False)
    except pywikibot.EditConflict:
        pywikibot.output('Edit conflict! skip!')
        raise
    except pywikibot.ServerError:
        pywikibot.output('Server Error! Wait..')
        pywikibot.sleep(config.retry_wait)
        return None
    except pywikibot.SpamfilterError as e:
        pywikibot.output(f'Cannot change {page.title()} because of blacklist entry {e.url}')
        raise
    except pywikibot.LockedPage:
        pywikibot.output(f'Skipping {page.title()} (locked page)')
        raise
    except pywikibot.PageNotSaved as error:
        pywikibot.output(f'Error putting page: {error.args}')
        raise
    return True
    


def add_text_to_wiki(textfile, wikipage):
    with codecs.open(textfile, 'r', config.textfile_encoding) as file:
        text = file.read()
    site = pywikibot.Site()
    page = pywikibot.Page(site, wikipage)
    put_text(page, text)

from SPARQLWrapper import SPARQLWrapper, JSON
import json
import os
import logging

WIKIDATA_ENDPOINT = 'https://query.wikidata.org/sparql'
HTTP_AGENT_HEADER = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 ' \
                    'Safari/537.11 '

IN_COUNTRY = 'P17'
INSTANCE_OF = 'P31'

SWITERLAND = 'Q39'
CASTLE = 'Q23413'
FORTIFICATION = 'Q57821'
CASTRUM = 'Q88205'
CHATEAU = 'Q751876'
ARCHITECTURAL_STRUCTURE = 'Q811979'
MANSION = 'Q1802963'
CASTLE_RUIN = 'Q17715832'
DEFENSIVE_TOWER = 'Q20034791'

QUERY = f'''SELECT DISTINCT ?id ?item ?itemLabel
WHERE {{
  {{
    ?item wdt:{IN_COUNTRY} wd:{SWITERLAND}.
    {{
      ?item wdt:{INSTANCE_OF} wd:{CASTLE}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{CHATEAU}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{CASTLE_RUIN}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{MANSION}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{CASTRUM}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{DEFENSIVE_TOWER}.
    }}
    UNION
    {{
      ?item wdt:{INSTANCE_OF} wd:{ARCHITECTURAL_STRUCTURE}.
      ?item wdt:{INSTANCE_OF} wd:{FORTIFICATION}.
    }}
    BIND(SUBSTR(STR(?item),STRLEN("http://www.wikidata.org/entity/")+1) AS ?id).
  }}
  SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE],de" }}
}}
ORDER BY ASC(?item)'''

DUMP_PATH = 'dumps'
DUMP_FILE = 'wd_castles.json'

logger = logging.getLogger('[castle-query-wd]')


class Error(Exception):
    pass


class WikidataQueryError(Error):
    pass


def _execute_query():
    sparql = SPARQLWrapper(WIKIDATA_ENDPOINT, agent=HTTP_AGENT_HEADER)
    sparql.setQuery(QUERY)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()


def _create_dump_directory():
    if not os.path.exists(DUMP_PATH):
        try:
            logger.info(f'Creating directory {DUMP_PATH}')
            os.mkdir(DUMP_PATH, 0o755)
        except OSError as e:
            logging.error('Creating directory failed')
            raise WikidataQueryError(e.message)


def _write_results_file(results):
    try:
        logger.info('Writing results into json file')
        with open(f'{DUMP_PATH}/{DUMP_FILE}', 'w+') as output:
            json.dump(results, output, indent=4)
            logger.info('Writing results successful')
    except OSError as e:
        logging.error('Writing results to json file failed')
        raise WikidataQueryError(e.message)


def query_wd_castles():
    query_results = _execute_query()
    _create_dump_directory()
    _write_results_file(query_results)

import json

INPUT_PATH = 'dumps'
INPUT_FILE = 'wd_castles.json'


def extract_tags_from_json():
    with open(f'{INPUT_PATH}/{INPUT_FILE}', 'r') as input_json:
        wd_dump = json.load(input_json)
    entries = wd_dump['results']['bindings']
    tags = []
    for entry in entries:
        tags.append(entry['id']['value'])
    return tags

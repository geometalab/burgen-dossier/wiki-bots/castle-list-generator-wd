import json
import os

INPUT_PATH = 'dumps'
INPUT_FILE_WD = 'wd_castles.json'
INPUT_FILE_OSM = 'osm_tagged_entities.json'

WIKI_PATH = 'wiki'
WIKI_FILE = 'wd_missing_osm_entities'


def headers():
    return ['! ID\n', '! Name\n']


def wikidata_rows(wd_entries_missing_osm_entity):
    entries = []
    for entry in wd_entries_missing_osm_entity:
        try:
            identifier = f'| [{str(entry["item"]["value"])} {str(entry["id"]["value"])}]\n'
        except KeyError:
            identifier = '| style="background-color: #FFEBAD" | -\n'
        try:
            name = f'| {entry["itemLabel"]["value"]} \n'
        except KeyError:
            name = '| style="background-color: #FFEBAD" | -\n'
        entries.append([identifier, name, '|-\n'])
    return [row for sublist in entries for row in sublist]


def process_data_dumps():
    with open(f'{INPUT_PATH}/{INPUT_FILE_WD}', 'r') as wd_json:
        wd_dump = json.load(wd_json)

    with open(f'{INPUT_PATH}/{INPUT_FILE_OSM}', 'r') as osm_json:
        osm_dump = json.load(osm_json)

    wd_entries = wd_dump['results']['bindings']
    osm_entries = osm_dump['elements']

    wd_entries_missing_osm_entity = []

    for wd_entry in wd_entries:
        tag = wd_entry['id']['value']
        entry_found = next((osm_entry for osm_entry in osm_entries if osm_entry['tags']['wikidata'] == tag), False)
        if not entry_found:
            wd_entries_missing_osm_entity.append(wd_entry)

    if not os.path.exists(WIKI_PATH):
        os.mkdir(WIKI_PATH, 0o755)

    with open(f'{WIKI_PATH}/{WIKI_FILE}', 'w+') as output:
        output.writelines(
            ['Die nachfolgende Tabelle enthält sämtliche Wikidata Einträge, die Burgen beschreiben und '
             'keine zugehörige OSM Entity existiert. Eine Query prüft die Wikidata Einträge auf '
             '"instanceof" (<code>wdt:P31</code>) '
             '"castle" (<code>wd:Q23413</code>), '
             '"chateau" (<code>wd:Q751876</code>), '
             '"castrum" (<code>wd:Q88205</code>), '
             '"castle ruin" (<code>wd:Q17715832</code>), '
             '"mansion" (<code>wd:Q1802963</code>), '
             '"defensive tower" (<code>wd:Q20034791</code>) und '
             '"architectural structure" (<code>wd:Q811979</code>) kombiniert mit '
             '"fortification" (<code>wd:Q57821</code>) '
             'und beschränkt die Resultate auf "in country" (<code>wdt:P17</code>) "Switzerland" (<code>wd:Q39</code>).'
             ' Eine weitere Query durchsucht OSM nach den erhaltenen Tags.\n\n '
             '{{Caution|Diese Seite wird automatisch aktualisiert mittels eines stündlich ablaufenden Scripts. Das '
             'manuelle Updaten von Tabelleneinträgen lohnt sich deshalb nicht, weil Änderungen wieder überschrieben '
             'werden. Sollten Probleme mit dem Skript auftreten, so können Issues im ['
             'https://gitlab.com/geometalab/castle-list-generator-wd Repository] erfasst werden.}}\n\n\n\n']
        )
        output.writelines(
            ['{| class="wikitable"\n',
             '|+ Caption: Wikidata Einträge ohne zugehörige OSM Einträge\n',
             '|-\n'])
        output.writelines(headers())
        output.writelines('|-\n')
        output.writelines(wikidata_rows(wd_entries_missing_osm_entity))
        output.writelines('|}')

import overpass
import os
import json
import logging

OVERPASS_ENDPOINT = 'http://overpass-api.de/api/interpreter'
TIMEOUT = 300

DUMP_PATH = 'dumps'
DUMP_FILE = 'osm_tagged_entities.json'

logger = logging.getLogger('[tagged-entities-query-osm]')


class Error(Exception):
    pass


class OverpassQueryError(Error):
    pass


def _execute_query(tags):
    tag_regex = ""
    for tag in tags:
        tag_regex += f'{tag}|'
    tag_regex = tag_regex[:-1]
    api = overpass.API(endpoint=OVERPASS_ENDPOINT, timeout=TIMEOUT)
    try:
        logger.info('Executing query on overpass api')
        results = api.get(
            f'nwr[wikidata~"{tag_regex}"];',
            responseformat='json')
    except Exception:
        logging.error('Query failed')
        raise OverpassQueryError()
    logger.info('Query execution successful')
    return results


def _create_dump_directory():
    if not os.path.exists(DUMP_PATH):
        try:
            logger.info(f'Creating directory {DUMP_PATH}')
            os.mkdir(DUMP_PATH, 0o755)
        except OSError:
            logging.error('Creating directory failed')
            raise OverpassQueryError()


def _write_results_file(results):
    try:
        logger.info('Writing results into json file')
        with open(f'{DUMP_PATH}/{DUMP_FILE}', 'w+') as output:
            json.dump(results, output, indent=4)
            logger.info('Writing results successful')
    except OSError:
        logging.error('Writing results to json file failed')
        raise OverpassQueryError()


def query_osm_entities_for_tags(tags):
    query_results = _execute_query(tags)
    _create_dump_directory()
    _write_results_file(query_results)

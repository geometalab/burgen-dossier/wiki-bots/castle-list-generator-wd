from castle_query_wikidata import query_wd_castles, WikidataQueryError
from tagged_entities_query_osm import query_osm_entities_for_tags, OverpassQueryError
from tag_filter import extract_tags_from_json
from missing_osm_entry_processor import process_data_dumps
from add_text_to_wiki import add_text_to_wiki

TEXTFILE = 'wiki/wd_missing_osm_entities'
WIKIPAGE = u'Wikimedia CH/Burgen-Dossier/FehlendeOSMEintraege'


def main():
    try:
        query_wd_castles()
        query_osm_entities_for_tags(extract_tags_from_json())
    except WikidataQueryError:
        return
    except OverpassQueryError:
        return

    process_data_dumps()
    add_text_to_wiki(TEXTFILE, WIKIPAGE)


if __name__ == "__main__":
    main()
